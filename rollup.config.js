import svelte from "rollup-plugin-svelte";
import image from '@rollup/plugin-image';
import resolve from "rollup-plugin-node-resolve";
import { terser } from "rollup-plugin-terser";
import postcss from "rollup-plugin-postcss";
import autoPreprocess from "svelte-preprocess";
import commonjs from "rollup-plugin-commonjs";
import livereload from "rollup-plugin-livereload";
import typescript from '@rollup/plugin-typescript';
const url = require("postcss-url");

const production = !process.env.ROLLUP_WATCH;

export default {
  input: production ? "src/index.js" : "./src/dev/test-app-index.js",
  output: {
    sourcemap: true,
    format: "iife",
    name: "app",
    file: production ? "dist/index.js" :"public/bundle.js"
  },
  plugins: [
    postcss({
      extract: true,
      plugins: [url({ url: "inline", maxSize: 10, fallback: "copy" })]
    }),
    svelte({
      // enable run-time checks when not in production
      dev: !production,
      // we'll extract any component CSS out into
      // a separate file — better for performance
      css: (css) => {
        css.write("public/bundle.css");
      },
      emitCss: false,
      preprocess: autoPreprocess()
    }),
    image(),
    commonjs(),
    resolve({
      dedupe: ['svelte']
    }),
    //typescript({ sourceMap: !production }),
    !production &&
    livereload({
      watch: "public",
      clientUrl: process.env.CLIENT_URL,
    }),
    // If we're building for production (npm run build
    // instead of npm run dev), minify
    production && terser()
  ]
};
