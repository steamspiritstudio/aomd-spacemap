type Ship = {
    id: number;
    callsign: string;
    coordinate_x: number;
    coordinate_y: number;
    name: string;
    picture?: string;
}

type Station = {
    id: number;
    name: string;
    coordinate_x: number;
    coordinate_y: number;
}

type Fairway = {
    central_coordinate_alpha_x : number;
    central_coordinate_alpha_y : number;
    central_coordinate_beta_x : number;
    central_coordinate_beta_y : number;
    move_range_alpha: number;
    move_range_beta : number;
    sector_id_alpha: number;
    sector_id_beta : number;
    sector_name_alpha: string;
    sector_name_beta: string;
}

interface SectorMap {
    ships: Ship[] | [];
    stations: Station[] | [];
    fairway: Fairway[] | [];
}