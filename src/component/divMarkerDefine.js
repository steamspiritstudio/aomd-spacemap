export let userMarker = {
    className: "leaflet-usermarker",
    iconSize: [34, 34],
    iconAnchor: [17, 17],
    popupAnchor: [0, -20],
    labelAnchor: [11, -3],
    html: '<i class="inner"></i>'
};
export let userMarkerSmall = {
    className: "leaflet-usermarker-small",
    iconSize: [17, 17],
    iconAnchor: [9, 9],
    popupAnchor: [0, -10],
    labelAnchor: [3, -4],
    html: '<i class="inner"></i>'
}