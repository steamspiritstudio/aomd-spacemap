import LeafletMap from "svelte-leafletjs/src/components/LeafletMap.svelte";

import Circle from "svelte-leafletjs/src/components/Circle.svelte";
import CircleMarker from "svelte-leafletjs/src/components/CircleMarker.svelte";
import Icon from "svelte-leafletjs/src/components/Icon.svelte";
import ImageOverlay from "svelte-leafletjs/src/components/ImageOverlay.svelte";
import Marker from "svelte-leafletjs/src/components/Marker.svelte";
import Popup from "svelte-leafletjs/src/components/Popup.svelte";
import Tooltip from "svelte-leafletjs/src/components/Tooltip.svelte";

import LeafletRotatedMarkersExtension from "svelte-leafletjs/src/extensions/RotatedMarkers";

LeafletRotatedMarkersExtension.install();

export {
    LeafletMap,
    Circle,
    CircleMarker,
    Icon,
    ImageOverlay,
    Marker,
    Popup,
    Tooltip
};
export default LeafletMap;
  