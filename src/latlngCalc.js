import {getContext} from 'svelte';
import L from 'leaflet';

const maxNativeZoom = 4;

export function convertPointToLatLng(latlngArray) {
    return [(latlngArray[0] + 1) * 50, (latlngArray[1] + 1) * 50];
}

export function point2LatLngForMarker(x, y) {
    const latLng = convertPointToLatLng([x, y]);
    latLng[0] += 25;
    latLng[1] += 30;
    return L.latLng(getContext(L).getMap().unproject(latLng, maxNativeZoom));
}

export function latlng2point(latLng) {
    const point = getContext(L).getMap().project(L.latLng(latLng), maxNativeZoom);
    point.x = point.x / 50 - 1;
    point.y = point.y / 50 - 1;
    return point;
}

export function point2LatLng(x, y) {
    const map = getContext(L).getMap();
    if (typeof y == "number") {
        return map.unproject(convertPointToLatLng([x, y]), maxNativeZoom);
    }
    if (x.length > 0) {
        return x.map(points =>
            map.unproject(convertPointToLatLng(points), maxNativeZoom)
        );
    }
    return map.unproject(convertPointToLatLng(x), maxNativeZoom);
}